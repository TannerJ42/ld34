﻿using UnityEngine;
using System.Collections;

public class rotator_controller : MonoBehaviour
{
    public Transform rotationTarget;
    public Transform radiusMarker;
    public Transform childObject;

    public Quaternion lastRotation;


    void Start()
    {
        transform.Rotate(Vector3.forward * Random.Range(0, 360));
        lastRotation = rotationTarget.rotation;
    }

    void FixedUpdate()
    {
        if (lastRotation != rotationTarget.rotation)
       {
            transform.Rotate(Vector3.forward * -Quaternion.Angle(rotationTarget.rotation, lastRotation));
            lastRotation = rotationTarget.rotation;
        }

        float radius = Vector2.Distance(rotationTarget.position, radiusMarker.position);
        Vector3 position = childObject.localPosition;
        position.x = 0;
        position.y = radius;
        childObject.localPosition = position;
    }
}
