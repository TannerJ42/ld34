﻿using UnityEngine;
using System.Collections;

public class walk_test : MonoBehaviour
{
    public float walkSpeed = 0f;

	// Use this for initialization
	void Start()
    {
        InvokeRepeating("changeDirection", 0, Random.Range(1,3));   
	}
	
	// Update is called once per frame
	void Update()
    {
	    
	}

    void FixedUpdate()
    {
        transform.parent.transform.Rotate(Vector3.forward * -walkSpeed);
    }

    void changeDirection()
    {
        walkSpeed = Random.Range(-0.5f, 0.5f);
    }
}
