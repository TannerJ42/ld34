﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ring_controller : MonoBehaviour
{
    static bool randomSet = false;
    public static bool rotateLeft;

    [HideInInspector]
    public float surfaceRadius;

    [HideInInspector]
    public ring_spawner spawner;

    public SpriteRenderer spriteRenderer;
    public Transform surfaceTransform;
    public Color color = Color.green;

    public GameObject nodePrefab;

    public Color[] colors;

    public GameObject onDestroyParticles;
    public float onDestroyParticlesTime = 3f;

    private float currentSize;
    public float startingSize = 0.1f;
    public float maxSize = 5f;
    public float scaleSpeed = 0.1f;
    private float rotationSpeed = 0f;
    [HideInInspector]
    public Vector3 rotationDirection;
    public float minRotationSpeed = 0.01f;
    public float maxRotationSpeed = 0.1f;

    private List<NodeController> nodes;
    public int nodesSpawned = 0;
    public int nodesSpawnedIndex;

    // Use this for initialization
    void Start()
    {
        if (!randomSet)
        {
            randomSet = true;
            rotateLeft = Random.Range(0, 2) == 1 ? true : false; 
        }

        Vector3 scale = transform.localScale;
        scale.x = startingSize;
        scale.y = startingSize;
        transform.localScale = scale;

        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);

        if (rotateLeft)
        {
            rotationDirection = Vector3.back;
        }
        else
            rotationDirection = Vector3.forward;
        rotateLeft = !rotateLeft;

        color = spriteRenderer.color;
        surfaceRadius = Vector2.Distance(transform.position, surfaceTransform.position);

        StartCoroutine(FadeTo(1.0f, 2.5f));

        currentSize = transform.localScale.x;

        nodes = new List<NodeController>();
    }
	
    public void onNodeDestroyed(NodeController node)
    {
        nodes.Remove(node);

        if (nodes.Count == 0)
            OnRingDestroyed();
    }

	// Update is called once per frame
	void Update()
    {
        surfaceRadius = Vector2.Distance(transform.position, surfaceTransform.position);

        if (nodesSpawnedIndex > 0 &&
            currentSize >= spawner.sizes[nodesSpawnedIndex])
        {

            for (int i = 0; i <= spawner.numOfNodes[nodesSpawnedIndex] - 1; i++)
                CreateNode();

            nodesSpawnedIndex--;
        }
    }

    void CreateNode()
    {
        if (nodePrefab == null)
            return;
        nodesSpawned++;
        GameObject obj = (GameObject)Instantiate(nodePrefab);
        NodeController cont = obj.GetComponentInChildren<NodeController>();
        cont.controller = this;
        nodes.Add(cont);
        obj.transform.Rotate(Vector3.forward * Random.Range(0, 36) * 10);
        RotateAroundRing rot = obj.GetComponentInChildren<RotateAroundRing>();
        if (rot != null)
            rot.SetRing(this);
    }

    void FixedUpdate()
    {
        currentSize = transform.localScale.x;

        if (transform.localScale.x < maxSize)
        {
            Vector3 scale = transform.localScale;
            scale.x = Mathf.Min(scale.x + scaleSpeed * Time.fixedDeltaTime, maxSize);
            scale.y = Mathf.Min(scale.x + scaleSpeed * Time.fixedDeltaTime, maxSize);
            transform.localScale = scale;
        }

        transform.Rotate(rotationDirection * rotationSpeed * Time.fixedDeltaTime);
    }

    public void OnRingDestroyed()
    {
        if (onDestroyParticles != null)
        {
            GameObject explosion = (GameObject)Instantiate(onDestroyParticles, transform.position, transform.rotation);
            Vector3 scale = explosion.transform.localScale;
            scale.x = transform.localScale.x * 2;
            scale.y = transform.localScale.y * 2;

            
            explosion.transform.localScale = scale;
            Destroy(explosion, onDestroyParticlesTime);
        }

        for (int i = 0; i <= nodes.Count - 1; i++)
        {
            Destroy(nodes[i].gameObject);
        }

        spawner.OnRingDestroyed();

        Destroy(gameObject);
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = spriteRenderer.color.a;

        for (float t= 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            color.a = Mathf.Lerp(alpha, aValue, t);
            spriteRenderer.color = color;
            yield return null;
        }
    }

    public NodeController[] getNodes()
    {
        if (nodes == null)
            return null;
        NodeController[] n = nodes.ToArray();
        return n;
    }
}
