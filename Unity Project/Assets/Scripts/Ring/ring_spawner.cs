﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ring_spawner : MonoBehaviour
{
    public GameObject ringPrefab;
    private List<ring_controller> _rings;

    public float[] sizes;
    public int[] numOfNodes;

    public float secondsBetweenRings = 10f;
    [Tooltip("From outer to inner")]
    public int maxNumberOfRings = 5;

	// Use this for initialization
	void Start() 
	{
        if (sizes.Length != maxNumberOfRings)
            throw new Exception("The sizes count does not match maxNumberOfRings!");

        if (numOfNodes.Length != maxNumberOfRings)
            throw new Exception("The numOfNodes count does not match maxNumberOfRings!");

        _rings = new List<ring_controller>();

        InvokeRepeating("CreateRing", 0, secondsBetweenRings);
	}
	
    void CreateRing()
    {
        if (_rings.Count >= maxNumberOfRings)
            return;

        if (ResourceManager.instance.AllRingsDestroyed)
            return;

        if (ringPrefab != null)
        {
            GameObject ring = Instantiate(ringPrefab);
            ring_controller cont = ring.GetComponent<ring_controller>();
            cont.spawner = this;
            cont.nodesSpawnedIndex = maxNumberOfRings - 1;
            _rings.Add(cont);
            setRingAttributes();
        }
    }

    public int GetRingCount()
    {
        return _rings.Count;
    }

    public void OnRingDestroyed()
    {
        _rings.RemoveAt(0);
        for (int i = 0; i <= _rings.Count - 1; i++)
        {
            _rings[i].maxSize = sizes[i];
        }

        if (_rings.Count == 0)
        {
            ResourceManager.instance.AllRingsDestroyed = true;
            Invoke("EndGame", 3f);
        }
    }

    public void EndGame()
    {
        if (ResourceManager.instance.RocketLaunched == false)
            UnityEngine.SceneManagement.SceneManager.LoadScene("Result_Loss");
        else
            UnityEngine.SceneManagement.SceneManager.LoadScene("Result_Win");
    }

    private void setRingAttributes()
    {
        for (int i = 0; i <= _rings.Count - 1; i++)
        {
            _rings[i].maxSize = sizes[i];
        }
    }

    public ring_controller getOuterRing()
    {
        if (_rings != null && _rings.Count > 0)
            return _rings[0];

        return null;
    }

}
