﻿using UnityEngine;
using System.Collections;

public class RotateAroundRing : MonoBehaviour
{
    ring_controller ring;
    Quaternion lastRotation;

    float offset = -999;

    public void SetRing(ring_controller ringController)
    {
        if (offset == -999)
            offset = transform.localPosition.y;


        ring = ringController;
        UpdatePosition();
    }

    void Start()
    {
        
    }


    void Update()
    {
        if (ring == null)
            return;
    }

    void FixedUpdate()
    {
        if (ring == null)
            return;

        UpdatePosition();
    }

    void UpdatePosition()
    {
        float radius = ring.surfaceRadius;
        Vector3 position = transform.position;
        position.x = 0;
        position.y = radius + offset;
        transform.localPosition = position;

        // rotate to match ring's rotation
        if (lastRotation != ring.transform.localRotation)
        {
            transform.parent.Rotate(ring.rotationDirection * Quaternion.Angle(ring.transform.rotation, lastRotation));
            lastRotation = ring.transform.localRotation;
        }
    }
}
