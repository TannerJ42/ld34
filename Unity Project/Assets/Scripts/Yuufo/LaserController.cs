﻿using UnityEngine;
using System.Collections;

public class LaserController : MonoBehaviour
{
    bool firing = false;
    Vector3 pos;

    public GameObject particle;

	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 vectorToTarget = pos - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = q;
    }

    public void Fire(NodeController node)
    {
        if (firing || node == null || node.gameObject == null)
            return;

        Invoke("StopFiring", 0.2f);
        gameObject.SetActive(true);

        this.pos = node.gameObject.transform.position;
        Vector3 vectorToTarget = node.gameObject.transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = q;

        float distance = 2.5f * Vector3.Distance(this.pos, transform.position);

        Vector3 scale = transform.localScale;
        scale.x = distance;
        transform.localScale = scale;

        Vector3 particleScale = particle.transform.localScale;
        scale.y = distance * 0.1f;
        scale.z = 0.5f;
        scale.x = 0.5f;
        particle.transform.localScale = scale;

        firing = true;
    }

    void StopFiring()
    {
        firing = false;
        gameObject.SetActive(false);
    }
}
// 0.5, 0.29, 0.5