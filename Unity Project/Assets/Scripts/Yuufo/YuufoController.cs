﻿using UnityEngine;
using System.Collections;

public class YuufoController : MonoBehaviour
{
    public GameObject explosion;
    public ring_spawner spawner;
    public ring_controller targetRing;
    public NodeController targetNode;

    public float travelDownSpeed = 5f;
    public float hoverSpeed = 3f;

    public float minHoverDistance = 1f;

    public float hp = 1;

    public int damage = 1;

    public int damageOnClick = 1;

    public float timeBetweenDamage = 3f;
    float nextDamageTime = 0f;

    public CircleCollider2D firingRange;

    State state = State.Idle;

    bool moveright = false;

    public LaserController laser;

	// Use this for initialization
	void Start() 
	{

        moveright = Random.Range(0, 2) == 1 ? true : false;

        spawner = GameObject.FindGameObjectWithTag("Ring Spawner").GetComponent<ring_spawner>();
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}

    void FixedUpdate()
    {
        switch (state)
        {
            case State.Idle:
                targetRing = spawner.getOuterRing();
                if (targetRing != null)
                {
                    state = State.TravelingToRing;
                }
                FaceCenter();
                break;
            case State.TravelingToRing:
                if (targetRing == null)
                    state = State.Idle;
                else if (transform.localPosition.y <= targetRing.surfaceRadius + minHoverDistance)
                {
                    state = State.FiringAtNode;
                }
                FaceCenter();
                break;
            case State.FiringAtNode:
                AcquireTarget();

                if (targetRing == null)
                    targetRing = spawner.getOuterRing();

                if (targetNode != null)
                {
                    if (firingRange.radius >= Vector3.Distance(targetNode.transform.position, transform.position))
                    {
                        FaceTarget();
                        FireOnTarget();
                    }
                    else
                    {
                        FaceCenter();
                    }
    
                }
                else
                {
                    FaceCenter();
                }
                break;
        }

        if (targetRing != null)
        {
            Vector3 pos2 = transform.localPosition;
            pos2.y -= travelDownSpeed * Time.fixedDeltaTime;

            if (pos2.y <= targetRing.surfaceRadius + minHoverDistance)
            {
                pos2.y = targetRing.surfaceRadius + minHoverDistance;
            }

            transform.localPosition = pos2;
        }

        if (state != State.Idle &&
            state != State.TravelingToRing)
        {
            int direction = moveright ? -1 : 1;

            transform.parent.Rotate(Vector3.forward * hoverSpeed * 1 * direction * Time.fixedDeltaTime);
        }
    }

    void FaceTarget()
    {
        Vector3 targetDir = targetNode.gameObject.transform.position - transform.position;

        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
        angle += 90;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void FaceCenter()
    {
        Vector3 targetDir = new Vector3(0, 0, 0) - transform.position;

        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
        angle += 90;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void AcquireTarget()
    {
        if (targetRing == null)
            return;

        NodeController[] nodes = targetRing.getNodes();

        if (nodes == null)
            return;

        float dist = 1000;
        int index = 1000;

        for (var i = 0; i <= nodes.Length - 1; i++)
        {
            if (nodes[i] == null)
                continue;

            float d = Vector2.Distance(transform.position, nodes[i].transform.position);
            int enemyIndex = System.Array.IndexOf(nodes, nodes[i].nodeType);

            if (enemyIndex < index || (enemyIndex == index && d < dist))
            {
                dist = d;
                index = System.Array.IndexOf(nodes, nodes[i].nodeType);
                targetNode = nodes[i];
            }
        }
    }

    private void FireOnTarget()
    {
        if (targetNode == null)
            return;

        if (nextDamageTime <= Time.timeSinceLevelLoad)
        {
            laser.Fire(targetNode);

            targetNode.TakeDamage(damage);
            nextDamageTime = Time.timeSinceLevelLoad + timeBetweenDamage;
        }


    }

    public void TakeDamage(int damage)
    {
        if (hp < 0)
            return;

        hp -= damage;

        if (hp < damage)
        {
            OnUFODestroyed();
            Destroy(this.gameObject);
        }
    }

    void OnMouseDown()
    {
        TakeDamage(damageOnClick);
    }

    void OnUFODestroyed()
    {
        GameObject exp = (GameObject)Instantiate(explosion, transform.position, transform.rotation);
        Destroy(exp, 10f);
    }

    void OnDestroy()
    {

    }

    enum State
    {
        Idle,
        TravelingToRing,
        TravelingToNode,
        FiringAtNode
    }
}
