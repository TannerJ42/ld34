﻿using UnityEngine;
using System.Collections;

public class NodeController : MonoBehaviour
{
    public SpriteRenderer nodeImage;
    public GameObject nodeSparkle;


    public GameObject constructionPrefab;
    public GameObject housePrefab;
    public GameObject harvesterPrefab;
    public GameObject militaryPrefab;
    public GameObject rocketPrefab;

    public ring_controller controller;

    private GameObject construction = null;
    private GameObject building = null;

    public NodeTypes nodeType = NodeTypes.Blank;
    private NodeTypes inProgressBuilding = NodeTypes.Blank;

    public float houseTime = 5;


    public float harvesterTime = 5;

    public float militaryTime = 5;

    public float rocketTime = 5;

    public int houseHP = 1;
    public int harvesterHP = 1;
    public int militaryHP = 1;
    public int rocketHP = 1;

    public NodeTypes buildOnClick = NodeTypes.House;

    public GameObject explosion;

    public int hp = 1;

	// Use this for initialization
	void Start() 
	{
    }

    void OnMouseOver()
    {
        if (building != null)
            return;
    }

    public void TakeDamage(int dmg)
    {
        hp -= dmg;

        if (hp <= 0)
        {
            OnBlowUp();
        }
    }

    void OnBlowUp()
    {
        if (controller != null)
            controller.onNodeDestroyed(this);

        if (building != null)
            Destroy(building.gameObject);

        if (explosion != null)
        {
            GameObject exp = (GameObject)Instantiate(explosion, transform.position, transform.rotation);
            Destroy(exp, 5f);
        }
        Destroy(transform.parent.gameObject);
    }

    void OnMouseExit()
    {
    }

    public void EnableCollider()
    {
        GetComponent<Collider2D>().enabled = true;
    }

    void OnMouseDown()
    {
        if (building != null)
            return;

        if (this == null || transform == null)
            return;

        ResourceManager.instance.ShowShop(transform.position, this);

        GetComponent<Collider2D>().enabled = false;
    }

    public bool BuildStructure(NodeTypes type)
    {

        if (!ResourceManager.instance.CanPurchase(type))
            return false;

        if (!ResourceManager.instance.BuyStructure(type))
            return false;

        float time;
        GameObject prefab;

        switch (type)
        {
            case NodeTypes.House:
                time = houseTime;
                prefab = housePrefab;
                break;
            case NodeTypes.Harvester:
                time = harvesterTime;
                prefab = harvesterPrefab;
                break;
            case NodeTypes.Military:
                time = militaryTime;
                prefab = militaryPrefab;
                break;
            case NodeTypes.Rocket:
                time = rocketTime;
                prefab = rocketPrefab;
                break;
            default:
                time = 0;
                prefab = null;
                break;
        }

        if (prefab == null)
            return false;



        nodeImage.enabled = false;
        nodeSparkle.SetActive(false);

        construction = (GameObject)Instantiate(constructionPrefab, transform.position, transform.rotation);
        construction.transform.parent = transform;
        building = (GameObject)Instantiate(prefab, transform.position, transform.rotation);
        building.transform.parent = transform;
        building.SetActive(false);
        nodeType = NodeTypes.Building;
        inProgressBuilding = type;
        Invoke("FinishBuilding", time);
        ResourceManager.instance.HideShop();
        return true;
    }

    private void WinGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Result_Win");
    }

    private void FinishBuilding()
    {
        
        if (inProgressBuilding == NodeTypes.Rocket && !ResourceManager.instance.RocketLaunched)
        {
            ResourceManager.instance.RocketLaunched = true;
            WinGame();
        }

        building.SetActive(true);
        construction.SetActive(false);
        Destroy(construction);
        nodeType = inProgressBuilding;

        switch (nodeType)
        {
            case NodeTypes.House:
                hp = houseHP;
                break;
            case NodeTypes.Harvester:
                hp = harvesterHP;
                break;
            case NodeTypes.Military:
                hp = militaryHP;
                break;
            case NodeTypes.Rocket:
                hp = rocketHP;
                break;
            default:
                break;
        }
    }

    public enum NodeTypes
    {
        Blank,
        Building,
        House,
        Military,
        Harvester,
        Rocket
    }
}
