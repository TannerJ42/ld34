﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MilitaryController : MonoBehaviour
{
    public List<YuufoController> enemies;

    public float timeBetweenShots = 3;

    private float timeNextShot = 0;

    public int damage = 1;

    public MilitaryLaserController laser;

    // Use this for initialization
    void Start() 
	{
        if (enemies == null)
            enemies = new List<YuufoController>();
    }
	
	// Update is called once per frame
	void Update() 
	{
        for (int i = enemies.Count - 1; i >= 0; i--)
        {
            if (enemies[i] == null || enemies[i].gameObject == null)
                enemies.RemoveAt(i);
        }

        if (enemies.Count > 0 && timeNextShot <= Time.timeSinceLevelLoad)
        {
            YuufoController enemy = enemies[Random.Range(0, enemies.Count - 1)];

            if (enemy != null)
            {
                laser.Fire(enemy);
                enemy.TakeDamage(damage);
                timeNextShot = Time.timeSinceLevelLoad + timeBetweenShots;
            }

        }

	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag != "UFO")
            return;

        if (enemies == null)
            enemies = new List<YuufoController>();

        YuufoController ufo = c.gameObject.GetComponentInChildren<YuufoController>();

        if (ufo && c.tag == "UFO" && !enemies.Contains(ufo))
        {
            Debug.Log("Added UFO in ontriggerenter");
            enemies.Add(ufo);
        }
    }

    void OnTriggerLeave2D(Collider2D c)
    {
        if (c.tag != "UFO" || enemies == null)
            return;

        YuufoController ufo = c.gameObject.GetComponentInChildren<YuufoController>();

        if (ufo && enemies.Contains(ufo))
        {
            Debug.Log("Removed UFO in ontriggerleave");
            enemies.Remove(ufo);
        }

    }
}
