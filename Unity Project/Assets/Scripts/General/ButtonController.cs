﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    public Scenes scene;

    public enum Scenes
    {
        None,
        Gameplay,
        Credits,
        MainMenu,
        HowTo,
        Quit
    }

	// Use this for initialization
	void Start() 
	{
		
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}

    void OnMouseDown()
    {
        if (scene != Scenes.None)
            GoToScene(scene);
    }

    void GoToScene(Scenes scene)
    {
        string name = "";

        switch (scene)
        {
            case Scenes.Gameplay:
                name = "Gameplay";
                break;
            case Scenes.Credits:
                name = "Credits";
                break;
            case Scenes.MainMenu:
                name = "Title Screen";
                break;
            case Scenes.HowTo:
                name = "HowTo";
                break;
            case Scenes.Quit:
                Application.Quit();
                break;
            default:
                break;
        }
        if (name != "")
            SceneManager.LoadScene(name);
    }
}
