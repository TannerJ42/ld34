﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MusicPlayer : MonoBehaviour
{
    public static MusicPlayer instance;

	// Use this for initialization
	void Start() 
	{
        DontDestroyOnLoad(transform.gameObject);

        if (instance!= null)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
	}
	
    void OnLevelWasLoaded()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Gameplay"))
        {
            instance = null;
            Destroy(this.gameObject);
        }
    }

	// Update is called once per frame
	void Update() 
	{
		
	}
}
