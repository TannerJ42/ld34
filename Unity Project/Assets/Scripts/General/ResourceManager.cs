﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance;

    public ShopController shop;

    public Text energyText;
    public Text popText;

    private int lastenergy;
    private int lastpop;

    public int energy;
    public int startingEnergy = 1;

    public int people;
    public int startingPeople = 1;

    public int HouseEnergyPrice = 1;
    public int HousePeoplePrice = 0;
    public int HarvesterEnergyPrice = 1;
    public int HarvesterPeoplePrice = 1;
    public int MilitaryEnergyPrice = 1;
    public int MilitaryPeoplePrice = 1;
    public int RocketEnergyPrice = 5;
    public int RocketPeoplePrice = 2;

    public bool RocketLaunched = false;
    public bool AllRingsDestroyed = false;

    // Use this for initialization
    void Start() 
	{
        if (instance != null)
            Destroy(instance.gameObject);
        instance = this;

        energy = startingEnergy;
        people = startingPeople;
        energyText.text = energy.ToString();
        popText.text = people.ToString();

        shop.gameObject.SetActive(false);
	}

    void Update()
    {
        if (energy != lastenergy)
        {
            lastenergy = energy;
            energyText.text = energy.ToString();
        }
        if (people != lastpop)
        {
            lastpop = people;
            popText.text = people.ToString();
        }
    }

    //void OnGUI()
    //{
    //    if (Application.isEditor)  // or check the app debug flag
    //    {
    //        GUIStyle style = new GUIStyle();
    //        style.normal.textColor = Color.green;
    //        GUI.Label(new Rect(5, 5, 50, 50), "Energy: " + energy, style);
    //        GUI.Label(new Rect(5, 25, 50, 50), "People: " + people, style);
    //    }
    //}

    public bool BuyStructure(NodeController.NodeTypes type)
    {
        if (!CanPurchase(type))
            return false;

        int energyCost = GetEnergyCost(type);
        int peopleCost = GetPeopleCost(type);

        energy -= energyCost;
        people -= peopleCost;

        return true;
    }

    public int GetEnergyCost(NodeController.NodeTypes type)
    {
        int energyCost;

        switch (type)
        {
            case NodeController.NodeTypes.House:
                energyCost = HouseEnergyPrice;
                break;
            case NodeController.NodeTypes.Harvester:
                energyCost = HarvesterEnergyPrice;
                break;
            case NodeController.NodeTypes.Military:
                energyCost = MilitaryEnergyPrice;
                break;
            case NodeController.NodeTypes.Rocket:
                energyCost = RocketEnergyPrice;
                break;
            default:
                energyCost = 1000;
                Debug.LogError("Bad switch argument for ResourceManager.GetEnergyCost");
                break;
        }

        return energyCost;
    }

    public int GetPeopleCost(NodeController.NodeTypes type)
    {
        int peopleCost;

        switch (type)
        {
            case NodeController.NodeTypes.House:
                peopleCost = HousePeoplePrice;
                break;
            case NodeController.NodeTypes.Harvester:
                peopleCost = HarvesterPeoplePrice;
                break;
            case NodeController.NodeTypes.Military:
                peopleCost = MilitaryPeoplePrice;
                break;
            case NodeController.NodeTypes.Rocket:
                peopleCost = RocketPeoplePrice;
                break;
            default:
                peopleCost = 1000;
                Debug.LogError("Bad switch argument for ResourceManager.GetPeopleCost");
                break;
        }

        return peopleCost;
    }

    public bool CanPurchase(NodeController.NodeTypes type)
    {
        int energyCost = GetEnergyCost(type);
        int peopleCost = GetPeopleCost(type);

        if (energy >= energyCost && people >= peopleCost)
            return true;
        else
            return false;
    }
	
    public void HideShop()
    {
        shop.gameObject.SetActive(false);
    }

    public void ShowShop(Vector3 location, NodeController node)
    {
        shop.gameObject.SetActive(true);

        if (shop.currentNode != null && shop.currentNode != node)
        {
            shop.currentNode.EnableCollider();
        }
        shop.currentNode = node;

        shop.OnShow();

        transform.position = location;
    }
}
